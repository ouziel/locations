import {DELETE_SELECTED_LOCATIONS} from 'constants/locations';
import {filterVisibles} from './filterVisibles';
import setDB from '../setDB';
function deleteSelectedLocations() {
    return (dispatch, getState) => {
        dispatch({type: DELETE_SELECTED_LOCATIONS})
        dispatch(setDB('locations'))
        dispatch(filterVisibles())

    }
}
export default deleteSelectedLocations;
