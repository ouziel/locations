import add, {openNew} from './addLocation';
import remove from './deleteLocation';
import edit from './editLocation';
import removeSelected from './deleteSelected';
import toggle from './toggleLocationIndex';
import saveEdited from './saveEdited';
import sortVisibles from './sortLocations';
import addFake from './addFake';
import showLocation from './showLocation';
import goBack from './goBack';
import {filterVisibles, showAll} from './filterVisibles';
export {
    goBack,
    openNew,
    add,
    remove,
    edit,
    toggle,
    saveEdited,
    filterVisibles,
    showAll,
    addFake,
    showLocation,
    removeSelected,
    sortVisibles
}
