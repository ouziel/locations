import addLocation from './addLocation';
import addFakeCategories from '../categories/addFake';
import {ADD_FAKE_LOCATIONS} from 'constants/locations';

function addFakeLocations() {

    return (dispatch, getState) => {

        const state = getState();
        const {categories, locations} = state;
        const categoriesData = categories.data;
        if (!categoriesData.size) {
            dispatch(addFakeCategories())
            dispatch(addFakeLocations());
        } else {
            dispatch({type: ADD_FAKE_LOCATIONS})
            const categoriesData = categories.data;
            const locationsData = locations.data;
            const max = 3;
            [...categoriesData].map(category => {
                let item;
                for (let i = 0; i < max; i++) {
                    const name = Math.floor(Math.random() * 9999).toString();
                    item = {
                        name: `Location number ${name}`,
                        category,
                        address: `some street num ${i}`,
                        latitude: '32.04286536919285',
                        longitude: '34.90003441661065'
                    }
                    dispatch(addLocation(item));
                }

            })
        }
    }

}
export default addFakeLocations;
