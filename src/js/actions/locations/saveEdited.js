import {SAVE_EDITED, FILTER_VISIBLES} from 'constants/locations';
import {browserHistory} from 'react-router';
import setDB from '../setDB';
import {filterVisibles} from './filterVisibles';
function saveEdited(item) {
    return (dispatch) => {
        dispatch({type: SAVE_EDITED, payload: {
                item
            }})
        dispatch(setDB('locations'))
        browserHistory.push({pathname: '/locations'});
        dispatch(filterVisibles(item.category))

    }
}
export default saveEdited;
