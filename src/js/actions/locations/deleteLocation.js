import {DELETE_LOCATION} from 'constants/locations';
import {filterVisibles} from './filterVisibles';
import setDB from '../setDB';
function deleteLocation(item) {

    return (dispatch, getState) => {

        dispatch({type: DELETE_LOCATION, payload: {
                item
            }})
        dispatch(setDB('locations'))
        dispatch(filterVisibles());

    }
}
export default deleteLocation;
