import {NEW_LOCATION, LOCATION_EXSITS, ADD_LOCATION, NO_CATEGORIES_CANT_ADD_LOCATION} from 'constants/locations';
import {browserHistory} from 'react-router';
import setDB from '../setDB';
import {filterVisibles} from './filterVisibles';

function openNew(name) {

    return (dispatch, getState) => {
        const state = getState();
        const categories = state.categories.data;
        if (!categories.size) {
            dispatch({type: NO_CATEGORIES_CANT_ADD_LOCATION})
        } else {
            dispatch({type: NEW_LOCATION})
            browserHistory.push({
                pathname: '/locations',
                query: {
                    add: true
                }
            });
        }
    }
}
function addLocation({category, name, address, latitude, longitude}) {

    return (dispatch, getState) => {

        const state = getState();
        const locations = state.locations.data;
        if (locations.some(x => (x.name === name && x.category === category))) {
            dispatch({type: LOCATION_EXSITS})
        } else {
            dispatch({
                type: ADD_LOCATION,
                payload: {
                    category,
                    name,
                    address,
                    latitude,
                    longitude
                }
            })
            dispatch(setDB('locations'))
            browserHistory.push({pathname: '/locations'});
            dispatch(filterVisibles(category));
        }

    }
}
export {openNew}
export default addLocation;
