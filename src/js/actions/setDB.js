import {SAVED_TO_DB} from 'constants/shared'
function setDB(key) {
    return (dispatch, getState) => {
        const state = getState();
        const {categories, locations} = state;
        let data;
        if (key === 'categories') {
            data = JSON.stringify([...categories.data]);
        } else {
            data = JSON.stringify(locations.data);

        }
        localStorage.setItem(key, data)
        dispatch({type: SAVED_TO_DB})
    }

}
export default setDB;
