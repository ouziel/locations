// TODO: add to LocalStorage
import {ADD_CATEGORY, CATEGORY_EXSITS} from 'constants/categories';
import setDB from '../setDB';

function addCategory(name) {

    return (dispatch, getState) => {

        const state = getState();
        const categories = state.categories.data;
        if (categories.has(name)) {
            dispatch({type: CATEGORY_EXSITS})
        } else {
            dispatch({type: ADD_CATEGORY, payload: {
                    name
                }})
            dispatch(setDB('categories'))
        }
    }
}

export default addCategory
