import add from './addCategory';
import toggle from './toggleCategoryIndex';
import edit from './editCategory';
import remove from './deleteCategory';
import removeSelected from './deleteSelected';
import categoryExsits from './categoryExsits';
import addFake from './addFake';
import sort from './sortCategories';
export {
    add,
    toggle,
    edit,
    addFake,
    remove,
    sort,
    removeSelected
}
