import {TOGGLE_CATEGORY_INDEX} from 'constants/categories';

function toggleCategoryIndex(index) {
    return {type: TOGGLE_CATEGORY_INDEX, payload: {
            index
        }}
}
export default toggleCategoryIndex
