import {SORT_CATEGORIES} from 'constants/categories';

function sortCategories() {
    return {type: SORT_CATEGORIES}
}
export default sortCategories
