import {EDIT_CATEGORY} from 'constants/categories'
import {CHANGE_LOCATION_CATEGORY} from 'constants/locations';

export default function editCategory({name, newName}) {

    return (dispatch) => {

        dispatch({
            type: EDIT_CATEGORY,
            payload: {
                name,
                newName
            }
        })

        dispatch({
            type: CHANGE_LOCATION_CATEGORY,
            payload: {
                name,
                newName
            }
        })
    }
}
