import React, {PropTypes} from 'react';
import List from 'components/List';
import Panel from 'muicss/lib/react/Panel';
import Container from 'muicss/lib/react/container';
import LocationsToolbar from 'components/Toolbar/locations';
import LocationItem from 'components/List/locationItem';
import Form from 'components/Form';
import PageTitle from 'components/PageTitle';
import LocationMap from 'components/Map';

export default class LocationsPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isAdding: false,
            isEditing: false,
            isViewing: false,
            query: {}
        }
    }
    componentDidMount() {
        this.props.showAll();
    }
    componentWillReceiveProps(nextProps) {

        const {location: {
                query
            }} = nextProps;

        if (query.add) {
            this.setState({isAdding: true})
        } else {
            this.setState({isAdding: false})
        }
        if (query.edit) {
            this.setState({isEditing: true})
            this.setState({query});

        } else {
            this.setState({isEditing: false})
        }

        if (query.view) {
            const {
                location: {
                    query: {
                        latitude,
                        longitude
                    }
                }
            } = nextProps;
            this.setState({isViewing: true, latitude, longitude})
        } else {
            this.setState({isViewing: false})
        }
    }

    renderLocationsToolbar() {

        const {openNew, removeSelected, sortVisibles, state, filterVisibles} = this.props;

        const {categories, locations} = state;
        return (<LocationsToolbar title='locations' categories={categories} locations={locations} remove={removeSelected} sort={sortVisibles} filter={filterVisibles} openNew={openNew}/>)
    }

    renderPanel() {
        const {
            add,
            remove,
            openNew,
            edit,
            toggle,
            removeSelected,
            saveEdited,
            state: {
                categories
            }
        } = this.props;

        const {
            state: {
                routing: {
                    locationBeforeTransitions: {
                        pathname
                    }
                }
            }
        } = this.props;

        const name = pathname.replace('/', '');
        const {isAdding, isEditing, isViewing, query} = this.state;

        if (isAdding) {

            return this.renderAddingForm({categories, add, remove, toggle});
        }
        if (isEditing) {

            return this.renderEditingForm({query, add, categories, saveEdited});

        }
        if (isViewing) {

            return this.renderViewMode()
        } else if (!isAdding && !isEditing && !isViewing) {

            return this.renderItems(name)
        }

    }
    renderViewMode() {

        return (
            <div className="view-page">

                <LocationMap disabled={true} longitude={this.state.longitude} latitude={this.state.latitude}/>

            </div>
        )
    }
    renderEditingForm({categories, query, saveEdited}) {

        return (<Form query={query} add={saveEdited} categories={categories.data}/>)
    }
    renderAddingForm({categories, add, remove, toggle}) {
        return (<Form categories={categories.data} add={add} remove={remove} toggle={toggle}/>)
    }
    renderItems(name) {
        const {edit, remove, toggle, addFake, showLocation} = this.props;

        return (
            <List addFake={addFake} name={name} items={this.props.state.locations.visibles}>
                <LocationItem remove={remove} show={showLocation} edit={edit} toggle={toggle}/>
            </List>
        )
    }
    renderEmptyToolbar() {
        const {isAdding, isEditing, isViewing} = this.state;
        const text = (isAdding && !isEditing)
            ? 'Add location'
            : (isViewing)
                ? 'View location'
                : 'Edit location';
        return (<LocationsToolbar goBack={this.props.goBack.bind(this)} title={text} blank={true}/>)
    }
    render() {
        const {isAdding, isEditing, isViewing} = this.state;
        return (
            <Container className="page">
                {(isAdding || isEditing || isViewing)
                    ? this.renderEmptyToolbar()
                    : this.renderLocationsToolbar()}
                <section className="list">
                    <Panel>
                        {this.renderPanel()}
                    </Panel>
                </section>
            </Container>
        );
    }
}
