import React, {PropTypes} from 'react';
import List from 'components/List';
import Button from 'muicss/lib/react/button';
import Input from 'muicss/lib/react/input';
import Panel from 'muicss/lib/react/Panel';
import Container from 'muicss/lib/react/container';
import CategoriesToolbar from 'components/Toolbar/categories';
import PageTitle from 'components/PageTitle';
import CategoryItem from 'components/List/CategoryItem';
import LocationMap from 'components/Map';

export default class CategoriesPage extends React.Component {
    constructor(props) {
        super(props);

    }
    render() {

        const {
            add,
            edit,
            remove,
            toggle,
            removeSelected,
            addFake,
            location: {
                pathname
            }
        } = this.props;

        const path = pathname.replace('/', '');
        return (
            <Container className="page">
                <CategoriesToolbar {...this.props} add={add} remove={removeSelected}/>
                <section className="list">
                    <Panel>
                        <List addFake={addFake} name={path} items={this.props.categories.data}>
                            <CategoryItem toggle={toggle.bind(this)} remove={remove.bind(this)} edit={edit.bind(this)}/>
                        </List>
                    </Panel>

                </section>
                <LocationMap/>
            </Container>
        );
    }
}
