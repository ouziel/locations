import React, {PropTypes} from 'react';
import LocationsPage from './locations';
import CategoriesPage from './categories';
import Icon from 'shared/Icon';
import Panel from 'muicss/lib/react/panel';
import Container from 'muicss/lib/react/container';
const Index = () => (
    <div className="Page">
        <Container>
            <Panel className="index-panel">

                <h1 className="mui--text-display4">
                    <Icon name="locations"/>
                    <span>Spots!</span>
                </h1>
                <h4 className="mui--text-display3">Save Categorized Locations</h4>
            </Panel>
        </Container>
    </div>
)

export {LocationsPage, CategoriesPage, Index};
