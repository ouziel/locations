import React, {PropTypes} from 'react';
import Button from 'muicss/lib/react/button';
import Icon from 'shared/Icon';
const Btn = (props) => {
    const {
        text,
        icon,
        onClick,
        color = 'primary'
    } = props;
    return (
        <Button style={props.style} color={color} onClick={onClick
            ? onClick.bind(this)
            : null}>
            <span className="desktop">
                {text}
            </span>
            <span className="mobile">
                <Icon name={icon}/>
            </span>
        </Button>
    )
}
export default Btn;
