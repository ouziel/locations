import React, {PropTypes} from 'react';
import {Link} from 'react-router';
import Icon from 'shared/Icon';
export default class Nav extends React.Component {
    render() {
        return (
            <nav className="navbar">
                <ul className="nav-list">
                    <li className="nav-item">

                        <Link to="/locations" activeClassName="active-link">
                            <Icon name="locations"/>
                            <span>locations</span>
                        </Link>
                    </li>
                    <li className="nav-item">

                        <Link to="/categories" activeClassName="active-link">
                            <Icon name="categories"/>
                            <span>
                                categories
                            </span>
                        </Link>
                    </li>
                </ul>
            </nav>
        );
    }
}
