import React, {PropTypes} from 'react';

const Symbols = () => {
    return (
        <svg className="symbols">
            <defs>
                <symbol id="icon_close" fill="inherit" height="24" viewBox="0 0 24 24" width="24">
                    <path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/>
                </symbol>

                <symbol id="icon_delete" fill="inherit" height="24" viewBox="0 0 24 24" width="24">
                    <path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z"/>
                </symbol>

                <symbol id="icon_add" fill="inherit" height="24" viewBox="0 0 24 24" width="24">
                    <path d="M19 13h-6v6h-2v-6H5v-2h6V5h2v6h6v2z"/>
                </symbol>

                <symbol id="icon_locations" fill="inherit" height="24" viewBox="0 0 24 24" width="24">
                    <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z"/>
                </symbol>
                <symbol id="icon_edit" fill="inherit" height="24" viewBox="0 0 24 24" width="24">
                    <path d="M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z"/>
                </symbol>

                <symbol id="icon_sort" fill="inherit" height="24" viewBox="0 0 24 24" width="24">
                    <path d="M14.94 4.66h-4.72l2.36-2.36zm-4.69 14.71h4.66l-2.33 2.33zM6.1 6.27L1.6 17.73h1.84l.92-2.45h5.11l.92 2.45h1.84L7.74 6.27H6.1zm-1.13 7.37l1.94-5.18 1.94 5.18H4.97zm10.76 2.5h6.12v1.59h-8.53v-1.29l5.92-8.56h-5.88v-1.6h8.3v1.26l-5.93 8.6z"/>
                </symbol>
                <symbol id="icon_categories" fill="inherit" height="24" viewBox="0 0 24 24" width="24">
                    <path d="M4 14h4v-4H4v4zm0 5h4v-4H4v4zM4 9h4V5H4v4zm5 5h12v-4H9v4zm0 5h12v-4H9v4zM9 5v4h12V5H9z"/>
                </symbol>

                <symbol id="icon_editLocations" fill="inherit" height="24" viewBox="0 0 24 24" width="24">
                    <path d="M12 2C8.14 2 5 5.14 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.86-3.14-7-7-7zm-1.56 10H9v-1.44l3.35-3.34 1.43 1.43L10.44 12zm4.45-4.45l-.7.7-1.44-1.44.7-.7c.15-.15.39-.15.54 0l.9.9c.15.15.15.39 0 .54z"/>
                </symbol>

                <symbol id="icon_map" fill="inherit" height="24" viewBox="0 0 24 24" width="23.9">
                    <path d="M20.5 3l-.16.03L15 5.1 9 3 3.36 4.9c-.21.07-.36.25-.36.48V20.5c0 .28.22.5.5.5l.16-.03L9 18.9l6 2.1 5.64-1.9c.21-.07.36-.25.36-.48V3.5c0-.28-.22-.5-.5-.5zM15 19l-6-2.11V5l6 2.11V19z"/>
                </symbol>

                <symbol id="icon_pin" fill="inherit" height="24" viewBox="0 0 24 24" width="24">
                    <path d="M18 8c0-3.31-2.69-6-6-6S6 4.69 6 8c0 4.5 6 11 6 11s6-6.5 6-11zm-8 0c0-1.1.9-2 2-2s2 .9 2 2-.89 2-2 2c-1.1 0-2-.9-2-2zM5 20v2h14v-2H5z"/>
                </symbol>

                <symbol id="icon_shock" fill="inherit" width="16px" height="20px" viewBox="0 0 16 20">
                    <path d="M8,0C3.582,0,0,3.582,0,8s3.582,8,8,8s8-3.582,8-8S12.418,0,8,0z M5,5c0-0.552,0.448-1,1-1s1,0.448,1,1   c0,0.552-0.448,1-1,1S5,5.552,5,5z M8,12c-1.105,0-2-0.895-2-2c0-1.105,0.895-2,2-2s2,0.895,2,2C10,11.105,9.105,12,8,12z M10,6   C9.448,6,9,5.552,9,5c0-0.552,0.448-1,1-1s1,0.448,1,1C11,5.552,10.552,6,10,6z"/>
                </symbol>

                <symbol id="icon_more" fill="inherit" height="24" viewBox="0 0 24 24" width="24">
                    <path d="M22 3H7c-.69 0-1.23.35-1.59.88L0 12l5.41 8.11c.36.53.97.89 1.66.89H22c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zM9 13.5c-.83 0-1.5-.67-1.5-1.5s.67-1.5 1.5-1.5 1.5.67 1.5 1.5-.67 1.5-1.5 1.5zm5 0c-.83 0-1.5-.67-1.5-1.5s.67-1.5 1.5-1.5 1.5.67 1.5 1.5-.67 1.5-1.5 1.5zm5 0c-.83 0-1.5-.67-1.5-1.5s.67-1.5 1.5-1.5 1.5.67 1.5 1.5-.67 1.5-1.5 1.5z"/>
                </symbol>

                <symbol id="icon_arrowRight" fill="inherit" height="24" viewBox="0 0 24 24" width="24">
                    <path d="M12 4l-1.41 1.41L16.17 11H4v2h12.17l-5.58 5.59L12 20l8-8z"/>
                </symbol>

            </defs>
        </svg>
    )
}
export default Symbols;
