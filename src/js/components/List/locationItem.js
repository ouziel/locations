import React, {PropTypes} from 'react';
import Icon from 'shared/Icon';
import {validate, clearField, pushToQueue} from 'util';
import ToggleBox from 'components/ToggleBox';
import isWindowWidthMobile from 'util/isMobile';

export default class LocationItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isEditing: false,
            checked: false,
            isOpen: false,
            isMobile: isWindowWidthMobile()
        }
    }
    componentDidMount() {
        window.addEventListener('resize', this.checkIfMobile.bind(this));

    }
    componentWillUnmount() {
        window.removeEventListener('resize', this.checkIfMobile.bind(this));

    }
    checkIfMobile() {
        const isMobile = isWindowWidthMobile();
        this.setState({isMobile, isOpen: false})
    }
    toggleCheckbox(item = this.props.item) {
        const checked = !this.state.checked;
        this.setState({checked});
        this.props.toggle(item);
        if (!checked && this.state.isOpen && isWindowWidthMobile()) {
            this.toggleItem();
        }
    }
    toggleItem() {

        this.setState({
            isOpen: !this.state.isOpen
        })
    }
    renderButtons() {
        const {item, remove, edit, show} = this.props;

        let styles = {
            display: (this.state.isMobile)
                ? (this.state.checked)
                    ? 'block'
                    : 'none' : 'block'
        }
        const stylesInfo = {
            display: (this.state.isMobile)
                ? 'none'
                : 'inline-block'
        }
        return (
            <div className="actions" style={styles}>
                <Icon name="map" onClick={() => show(item)}/>
                <Icon name="more" style={stylesInfo} onClick={this.toggleItem.bind(this)}/>
                <Icon name="edit" onClick={() => edit(item)}/>
                <Icon name="delete" onClick={() => remove(item)}/>
            </div>

        )
    }
    render() {
        const {item} = this.props;
        return (
            <li className="category-item">
                <span className="checkbox">
                    <input checked={this.state.checked} ref={item.name} type="checkbox" onClick={this.toggleCheckbox.bind(this, item.name)}/>
                </span>

                <span className="text" ref="text">
                    <span>{item.name}</span>

                    {(!this.state.isMobile || this.state.isOpen) && <ToggleBox open={this.state.isOpen}>
                        <div className="item-info">
                            <div className="row">
                                <span>Category:</span>
                                <span>{item.address}</span>
                            </div>
                            <div className="row">
                                <span>Adddress:</span>
                                <span>{item.category}</span>
                            </div>
                        </div>
                    </ToggleBox>}
                </span>

                {this.renderButtons()}
            </li>
        );
    }
}
