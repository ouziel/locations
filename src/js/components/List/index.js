import React, {PropTypes} from 'react';
import Icon from 'shared/Icon';
export default class List extends React.Component {
    constructor(props) {
        super(props);

    }
    renderEmpty() {
        let {name} = this.props || 'FOO';
        name = `${name[0].toUpperCase()}${name.slice(1)}`;
        return (
            <div className="empty" onClick={this.props.addFake.bind(this)}>
                <Icon name="shock"/>
                <h1>
                    <span>No</span>
                    <span className="name">{name}</span>
                    <span className="hint">(click the icon to load fake data)</span>
                </h1>
            </div>
        )
    }
    renderItems(items, children) {

        return (items.map((item, key) => {

            const {onClick} = children.props;
            return React.cloneElement(children, {
                key: (typeof item === 'string')
                    ? item
                    : item.name,
                item,
                onClick
            })
        }))
    }
    componentDidMount() {}
    render() {

        let {items, children} = this.props;
        items = [...items];

        return (

            <ul>
                {(items.length)
                    ? this.renderItems(items, children)
                    : this.renderEmpty()}
            </ul>

        );
    }
};
