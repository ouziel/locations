import React, {PropTypes} from 'react';
import Icon from 'shared/Icon';
import {validate, clearField, pushToQueue} from 'util';
export default class CategoryItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isEditing: false,
            checked: false

        }
    }

    handleForm(e) {
        e.preventDefault();
        const field = this.refs.categoryInput
        const checkbox = this.refs.checkbox;
        this.props.edit({newName: field.value, name: this.props.item})
        clearField(field)

        this.toggleEditMode();
    }
    toggleEditMode() {
        const isEditing = !this.state.isEditing;
        const {categoryInput, categoryText} = this.refs;
        this.setState({isEditing})
        if (isEditing) {
            pushToQueue(() => categoryInput.focus());
        } else {
            categoryText.focus();
        }
    }

    toggleCheckbox(item = this.props.item) {

        const checked = !this.state.checked;
        this.setState({checked});
        this.props.toggle(item);
    }

    render() {
        const {item, id, toggle, edit, remove} = this.props;
        const {isEditing} = this.state;

        return (
            <li className="category-item">
                <span className="checkbox">
                    <input checked={this.state.checked} ref={item} type="checkbox" onClick={this.toggleCheckbox.bind(this, item)}/>
                </span>
                <form className="text" onSubmit={this.handleForm.bind(this)}>
                    <span ref="categoryText" style={{
                        'display': isEditing
                            ? 'none'
                            : 'block'
                    }}>
                        {item}
                    </span>
                    <input type="submit" hidden/>
                    <input className="input-text" type="text" ref="categoryInput" style={{
                        'display': isEditing
                            ? 'inline-block'
                            : 'none'
                    }}/>

                </form>
                <div className="actions">
                    <Icon name="edit" onClick={this.toggleEditMode.bind(this)}/>
                    <Icon name="delete" onClick={() => remove(item)}/>
                </div>
            </li>
        );
    }
}
