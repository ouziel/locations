import React, {PropTypes} from 'react';
const PageTitle = (props) => <h1 className="page-title" style={props.style}>{props.text}</h1>;
export default PageTitle;
