import React, {PropTypes} from 'react';

const Icon = ({name, onClick, style}) => {
    return (
        <div className="svg-icon" onClick={(onClick)
            ? onClick
            : null} style={style}>
            <svg>
                <use href={'#icon_' + name}></use>
            </svg>
        </div>
    )
}
export default Icon
