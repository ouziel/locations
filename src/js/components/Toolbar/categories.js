import React, {PropTypes} from 'react';
import Button from 'muicss/lib/react/button';
import {validate, clearField} from 'util';
import Btn from 'components/Btn';
import PageTitle from 'components/PageTitle';
export default class Toolbar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isAdding: false
        }
    }
    handleForm(e) {
        e.preventDefault();
        const field = this.refs.field
        if (validate(field)) {
            this.props.add(field.value)
            clearField(field)
            this.toggleAddingBox();
        } else {
            clearField(field, true)
        }
    }

    toggleAddingBox() {

        this.setState({
            isAdding: !this.state.isAdding
        })
    }

    render() {
        const {
            categories: {
                indexes,
                data
            }
        } = this.props;

        const stylesAdding = {
            display: this.state.isAdding
                ? 'block'
                : 'none'
        }
        const stylesHideWhileAdding = {
            display: this.state.isAdding
                ? 'none'
                : 'block'
        }

        const stylesDelete = {
            display: (!indexes.size || this.state.isAdding)
                ? 'none'
                : 'block'
        }

        const stylesSort = {
            display: (data.size > 1 && !this.state.isAdding)
                ? 'block'
                : 'none'
        }
        return (
            <nav className="toolbar">
                <div className="inner">
                    <PageTitle text="categories" style={stylesHideWhileAdding}/>
                    <div className="buttons">
                        <Btn style={stylesDelete} text="Delete" color='danger' icon="delete" onClick={this.props.remove.bind(this)}/>
                        <Btn style={stylesSort} text="Sort" color='accent' icon="sort" onClick={() => this.props.sort()}/>
                        <Btn style={stylesAdding} color='danger' text="X" icon="close" onClick={this.toggleAddingBox.bind(this)}/>
                        <form className="toolbar-form" onSubmit={this.handleForm.bind(this)}>
                            <input style={stylesAdding} type="text" ref="field" placeholder="Type a name" className="input-text"/>
                            <Btn style={stylesAdding} text="Add" icon="add" onClick={this.handleForm.bind(this)}/>
                            <Btn style={stylesHideWhileAdding} text="New" icon="add" onClick={this.toggleAddingBox.bind(this)}/>
                            <input type="submit" hidden/>
                        </form>
                    </div>
                </div>
            </nav>

        );
    }
}

Toolbar.propTypes = {};
