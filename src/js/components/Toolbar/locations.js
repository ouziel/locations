import React, {PropTypes} from 'react';
import Btn from 'components/Btn';
import {validate, clearField} from 'util';
import Icon from 'shared/icon';
import Dropdown from 'muicss/lib/react/dropdown';
import DropdownItem from 'muicss/lib/react/dropdown-item';
import PageTitle from 'components/PageTitle';

export default class LocationsToolbar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            category: ''
        }
    }
    changeCategory(category) {
        this.setState({category})
        this.props.filter(category);
    }
    componentDidMount() {
        this.changeCategory('Categories');
    }

    renderDropdown({currentCategory, categories}) {
        return (
            <span className="drop-down">
                <Dropdown label={(currentCategory)
                    ? currentCategory
                    : 'All'} ref="category">
                    <DropdownItem link="#" key='All' ref='all' onClick={this.changeCategory.bind(this, 'all')}>All</DropdownItem>
                    {[...categories.data].map(category => {
                        return (
                            <DropdownItem link="#" key={category} ref={category} onClick={this.changeCategory.bind(this, category)}>{category}</DropdownItem>
                        )
                    })}
                </Dropdown>
            </span>
        )
    }
    renderButtons() {

        const {
            remove,
            sort,
            openNew,
            categories,
            title,
            locations
        } = this.props;

        const {
            locations: {
                indexes,
                data,
                visibles,
                currentCategory
            }
        } = this.props;

        const stylesDelete = {
            display: (!indexes.size)
                ? 'none'
                : 'block'
        }
        const stylesSort = {
            display: (visibles.length > 1)
                ? 'block'
                : 'none'
        }

        return (
            <div className="toolbar-buttons">
                {data.length
                    ? this.renderDropdown({currentCategory, categories})
                    : null}
                <div className="buttons">
                    <Btn style={stylesDelete} text="Delete" color='danger' icon="delete" onClick={remove}/>
                    <Btn style={stylesSort} text="Sort" color='accent' icon="sort" onClick={sort}/>
                    <Btn text="New" icon="add" onClick={openNew}/>
                </div>
            </div>

        )
    }
    render() {

        const {title, blank, goBack} = this.props;
        return (

            <nav className="toolbar">
                <div className="inner">
                    <PageTitle text={title}/> {!blank && this.renderButtons()}
                    {blank && <span className="go-back">
                        <Icon name="arrowRight" onClick={goBack}/>
                    </span>}
                </div>
            </nav>

        );
    }
}
