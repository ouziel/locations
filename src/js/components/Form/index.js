import React, {PropTypes} from 'react';
import {validate, clearField} from 'util';
import LocationMap from 'components/Map';

export default class Form extends React.Component {
    constructor(props) {
        super(props);

        // new location state
        const blank = {
            name: '',
            address: '',
            longitude: '',
            latitude: '',
            category: ''
        }

        // edit location  state
        const {query} = this.props;
        /// assign state
        const state = (query)

            ? {
                ...query
            }
            : blank
        this.state = state;
    }

    submitForm(e) {
        e.preventDefault();
        const {category, name, address, latitude, longitude} = this.refs;
        let fieldsArray = Object.keys(this.refs).map(ref => {
            const field = this.refs[ref];
            if (!validate(field)) {
                field.focus();
                clearField(field)
                console.log('foo')
                return false;
            } else {
                return true;
            }

        });
        // if valid..
        if (!fieldsArray.some(x => x === false)) {
            this.props.add({category: category.value, name: name.value, address: address.value, latitude: latitude.value, longitude: longitude.value})
        };
    }

    onChange(ref) {
        this.setState({[ref]: this.refs[ref].value})
    }

    changeLatLong({latitude, longitude}) {
        this.setState({latitude, longitude});
    }
    render() {

        const {categories, query} = this.props;
        const {longitude, latitude} = this.state;
        return (
            <div className="edit-panel">
                <LocationMap longitude={longitude} latitude={latitude} change={this.changeLatLong.bind(this)}/>
                <form className="new-location" onSubmit={this.submitForm.bind(this)}>
                    {categories && <div className="row">
                        <select value={this.state.category} ref="category" onChange={this.onChange.bind(this, 'category')}>
                            {[...categories].map((category, i) => {
                                return (<option value={category} key={i} label={category}/>)
                            })}
                        </select>
                    </div>}

                    <div className="row">
                        <label>
                            Name of Location
                        </label>
                        <input type="text" ref="name" value={this.state.name} onChange={this.onChange.bind(this, 'name')}/>
                    </div>
                    <div className="row">
                        <label for="address">
                            Address
                        </label>
                        <input type="text" ref="address" ref="address" value={this.state.address} onChange={this.onChange.bind(this, 'address')}/>
                    </div>
                    <div className="row">
                        <div className="row">
                            <label>
                                Longitude
                            </label>
                            <input disabled className="longitude" type="number" value={this.state.longitude} ref="longitude" onChange={this.onChange.bind(this, 'longitude')}/>

                        </div>
                        <div className="row">
                            <label>
                                Latitude
                            </label>
                            <input disabled type="number" ref="latitude" value={this.state.latitude} onChange={this.onChange.bind(this, 'latitude')}/>
                        </div>

                    </div>
                    <div className="row">
                        <input type="submit" value="submit" className="submit-btn" required/>
                    </div>
                </form>
            </div>
        );
    }
}

Form.propTypes = {};
