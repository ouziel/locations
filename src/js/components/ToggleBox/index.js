import React, {PropTypes} from 'react';

export default class ToggleBox extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        const {open} = this.props;
        const styles = {
            height: (open)
                ? 'auto'
                : 0
        }
        return (
            <div className="toggle-box" style={styles}>
                {open
                    ? this.props.children
                    : null}
            </div>

        );
    }
}

ToggleBox.propTypes = {};
