import React, {PropTypes} from 'react';
import GoogleMap from 'google-map-react';
import config from 'config';
import Icon from 'shared/Icon';
const K_WIDTH = 40;
const K_HEIGHT = 40;
const markerStyles = {
    width: K_WIDTH,
    height: K_HEIGHT,
    left: -K_WIDTH / 2,
    top: -K_HEIGHT / 2,
    borderRadius: K_HEIGHT
};

const Marker = ({lat, long}) => {
    return (
        <div className="marker" style={markerStyles}>
            <Icon name="pin"></Icon>
        </div>
    )
}
export default class Map extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            latitude: '',
            longitude: ''
        }

    }
    onClick({x, y, lat, lng, event}) {

        this.setState({longitude: lng, latitude: lat})
        this.props.change({latitude: lat, longitude: lng})
    }

    componentDidMount() {
        const {latitude, longitude} = this.props;
        this.setState({latitude, longitude})
    }
    renderSingleMarker() {

        return (<Marker lat={this.state.latitude} lng={this.state.longitude}/>)

    }
    render() {
        const {disabled} = this.props;
        return (

            <div className="map">

                <GoogleMap onClick={disabled
                    ? null
                    : this.onClick.bind(this)} bootstrapURLKeys={{
                    key: config.googleKEY
                }} defaultCenter={this.props.center} defaultZoom={this.props.zoom}>

                    {this.renderSingleMarker()}
                </GoogleMap>

            </div>
        );
    }
}

Map.defaultProps = {
    center: {
        lat: 32.07788995831238,
        lng: 34.78467797129815
    },
    zoom: 9
}
Map.propTypes = {};
