import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as actionCreators from 'actions/locations';
/*
	View
*/
import View from 'pages/locations';
/*
make the new state avaiable via props
*/
function mapStateToProps(state) {
	return {state};
}
/*
	make the actions available via props
*/
export function mapDispatchToProps(dispatch) {
	return bindActionCreators(actionCreators, dispatch);
}
const Container = connect(mapStateToProps, mapDispatchToProps)(View);
export default Container;
