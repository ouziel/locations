import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as actionCreators from 'actions/categories';
/*
	View
*/
import View from 'pages/categories';
/*
make the new state avaiable via props
*/
function mapStateToProps({categories}) {
    return {categories};
}
/*
	make the actions available via props
*/
export function mapDispatchToProps(dispatch) {
    return bindActionCreators(actionCreators, dispatch);
}
const Container = connect(mapStateToProps, mapDispatchToProps)(View);
export default Container;
