import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
// import * as actionCreators from 'actions';

/*
	View
*/
import View from 'components/App/index.js';
/*
make the new state avaiable via props
*/
function mapStateToProps(state) {
	return {state};
}
const Container = connect(mapStateToProps)(View);
export default Container;
