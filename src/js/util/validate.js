export default function validate(el) {

    if (!el || !el.nodeName)
        return false;

    const type = el.getAttribute('type') || 'text',
        value = el.value;

    let pattern;
    switch (type) {
        case 'text':
            pattern = /^[\d a-zA-Z ]+$/;
            break;
        case 'number':
            pattern = /^[\d\.]+$/
            break;
        default:
            pattern = '';
    }

    const result = (pattern.test(value))
        ? true
        : false;

    return result;

}
