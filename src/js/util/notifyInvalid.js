export default function notifyInvalid(elm) {
    if (elm) {
        elm.value = '';
        elm.focus();
    }
}
