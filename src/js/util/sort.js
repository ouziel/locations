const sort = ((a, b) => a.name.toLowerCase() > b.name.toLowerCase())
export default sort;
