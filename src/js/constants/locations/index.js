const ADD_LOCATION = 'ADD_LOCATION';
const DELETE_LOCATION = 'DELETE_LOCATION';
const EDIT_LOCATION = 'EDIT_LOCATION';
const DELETE_SELECTED_LOCATIONS = 'DELETE_SELECTED_LOCATIONS';
const TOGGLE_LOCATION_INDEX = 'TOGGLE_LOCATION_INDEX';
const NEW_LOCATION = 'NEW_LOCATION';
const LOCATION_EXSITS = 'LOCATION_EXSITS';
const SAVE_EDITED = 'SAVE_EDITED';
const FILTER_VISIBLES = 'FILTER_VISIBLES';
const SHOW_ALL = 'SHOW_ALL';
const INITIAL_VISIBLES = 'INITIAL_VISIBLES';
const CHANGE_LOCATION_CATEGORY = 'CHANGE_LOCATION_CATEGORY';
const REMOVE_CATEGORY_LOCATIONS = 'REMOVE_CATEGORY_LOCATIONS';
const NO_CATEGORIES_CANT_ADD_LOCATION = 'NO_CATEGORIES_CANT_ADD_LOCATION';
const REFRESH_VISIBLES = 'REFRESH_VISIBLES';
const CHANGE_CURRENT_CATEGORY = 'CHANGE_CURRENT_CATEGORY';
const SORT_LOCATIONS = 'SORT_LOCATIONS';
const ADD_FAKE_LOCATIONS = 'ADD_FAKE_LOCATIONS';
const SHOW_LOCATION_ON_MAP = 'SHOW_LOCATION_ON_MAP';
const GO_BACK = 'GO_BACK';
export {
    GO_BACK,
    LOCATION_EXSITS,
    SORT_LOCATIONS,
    SHOW_ALL,
    INITIAL_VISIBLES,
    SAVE_EDITED,
    FILTER_VISIBLES,
    NEW_LOCATION,
    ADD_LOCATION,
    EDIT_LOCATION,
    DELETE_LOCATION,
    TOGGLE_LOCATION_INDEX,
    DELETE_SELECTED_LOCATIONS,
    CHANGE_LOCATION_CATEGORY,
    REMOVE_CATEGORY_LOCATIONS,
    REFRESH_VISIBLES,
    CHANGE_CURRENT_CATEGORY,
    NO_CATEGORIES_CANT_ADD_LOCATION,
    ADD_FAKE_LOCATIONS,
    SHOW_LOCATION_ON_MAP
};
