import {
    ADD_LOCATION,
    SAVE_EDITED,
    DELETE_LOCATION,
    EDIT_LOCATION,
    DELETE_SELECTED_LOCATIONS,
    TOGGLE_LOCATION_INDEX,
    FILTER_VISIBLES,
    SHOW_ALL,
    CHANGE_LOCATION_CATEGORY,
    REMOVE_CATEGORY_LOCATIONS,
    REFRESH_VISIBLES,
    CHANGE_CURRENT_CATEGORY,
    SORT_LOCATIONS
} from 'constants/locations';
const initialState = {
    data: [],
    visibles: [],
    indexes: new Set(),
    lastIndexEdited: '',
    currentCategory: ''
};

const LocationsReducer = (state = initialState, action) => {
    switch (action.type) {
        case CHANGE_CURRENT_CATEGORY:
            {

                const currentCategory = action.payload.category;
                return {
                    ...state,
                    currentCategory
                }

            }
        case REFRESH_VISIBLES:
            {

                return state;

            }
        case REMOVE_CATEGORY_LOCATIONS:
            {

                const data = state.data.filter(location => location.category !== action.payload.name);
                return {
                    ...state,
                    data
                }
            }
        case CHANGE_LOCATION_CATEGORY:
            {
                const data = state.data.map(location => {
                    if (location.category === action.payload.name) {
                        location.category = action.payload.newName;
                    }
                    return location;
                })
                return state;

            }
        case SORT_LOCATIONS:
            {
                const visibles = state.visibles.sort((a, b) => a.name.toLowerCase() > b.name.toLowerCase());
                return {
                    ...state,
                    visibles
                }

            }
        case SHOW_ALL:
            {
                const visibles = state.data;
                return {
                    ...state,
                    visibles
                }

            }
        case FILTER_VISIBLES:
            {
                const category = state.currentCategory,
                    data = state.data;
                const visibles = data.filter(item => item.category === category);
                return {
                    ...state,
                    visibles
                }
            }
        case SAVE_EDITED:
            {
                const data = [...state.data].map(location => {
                    if (location.name === state.lastIndexEdited) {
                        return action.payload.item;
                    }
                    return location;
                })
                return {
                    ...state,
                    data,
                    lastIndexEdited: ''
                }
            }
        case EDIT_LOCATION:
            {
                return {
                    ...state,
                    lastIndexEdited: action.payload.item.name
                }
            }
        case TOGGLE_LOCATION_INDEX:
            {
                let indexes = state.indexes;
                const index = action.payload.index;
                if (indexes.has(index)) {
                    indexes.delete(index)
                } else {
                    indexes = indexes.add(index);
                }
                return {
                    ...state,
                    indexes
                }
            }
        case DELETE_SELECTED_LOCATIONS:
            {
                const data = [...state.data].filter(item => !state.indexes.has(item.name));
                state.indexes.clear(); //  clears the state indexes...

                return {
                    ...state,
                    data
                }
            }

        case DELETE_LOCATION:
            {
                const data = [...state.data].filter(item => item.name !== action.payload.item.name);
                state.indexes.delete(action.payload.item.name)
                return {
                    ...state,
                    data
                }
            }
        case ADD_LOCATION:
            return {
                ...state,
                data: [
                    ...state.data,
                    action.payload
                ]
            }
        default:
            return state;
    }
}
export default LocationsReducer;;;
