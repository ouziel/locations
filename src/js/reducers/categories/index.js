import {
    ADD_CATEGORY,
    SORT_CATEGORIES,
    EDIT_CATEGORY,
    DELETE_CATEGORY,
    DELETE_SELECTED_CATEGORIES,
    TOGGLE_CATEGORY_INDEX
} from 'constants/categories';

const initialState = {
    data: new Set(),
    indexes: new Set()
};

const CategoriesReducer = (state = initialState, action) => {
    switch (action.type) {

        case SORT_CATEGORIES:
            {
                const data = new Set([...state.data].sort((a, b) => a.toLowerCase() > b.toLowerCase()));
                return {
                    ...state,
                    data
                }
            }
        case EDIT_CATEGORY:
            {
                state.data.delete(action.payload.name);
                const data = state.data.add(action.payload.newName);
                return {
                    ...state,
                    data
                };
            }

        case DELETE_CATEGORY:
            {
                state.data.delete(action.payload.name);
                state.indexes.delete(action.payload.name);
                const data = state.data;
                return {
                    ...state,
                    data
                };
            }
        case ADD_CATEGORY:
            {
                const data = state.data.add(action.payload.name);
                return {
                    ...state,
                    data
                }
            }
        case TOGGLE_CATEGORY_INDEX:
            {
                let indexes = state.indexes;
                const index = action.payload.index;
                if (indexes.has(index)) {
                    indexes.delete(index)
                } else {
                    indexes = indexes.add(index);
                }
                return {
                    ...state,
                    indexes
                }
            }

        case DELETE_SELECTED_CATEGORIES:
            let data = state.data;
            const indexes = state.indexes;
            // clear data
            data = new Set([...data].filter(item => !indexes.has(item)));
            // clear indexes
            indexes.clear(); //  clears the actuall state indexes...
            return {
                ...state,
                data
            }

        default:
            return state;
    }
}
export default CategoriesReducer;
