import {combineReducers} from 'redux'
import {routerReducer} from 'react-router-redux';
import CategoriesReducer from './categories';
import LocationsReducer from './locations';

const rootReducer = combineReducers({categories: CategoriesReducer, locations: LocationsReducer, routing: routerReducer})
export default rootReducer
