import {createStore, applyMiddleware, compose} from 'redux';
import {syncHistoryWithStore} from 'react-router-redux';
import {browserHistory} from 'react-router';
import rootReducer from './js/reducers';
import thunk from 'redux-thunk';

const enhancers = compose(applyMiddleware(thunk), window.devToolsExtension
	? window.devToolsExtension()
	: f => f);


let store = createStore(rootReducer, {}, enhancers);
export const history = syncHistoryWithStore(browserHistory, store);
export default store;
