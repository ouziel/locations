# Simple Starter Suite

### a React+React Router Kit.

- Deploy to Firebase (requires `firebase-tools`)
- Develop with a server for LAN `http://{ip}:{port}`
- ES6 (+ ...rest/spread + babel-polyfill)
- SASS + POSTCSS
  + reset
  + mixins
  + autoprefixer

### Usage
`npm run dev` - local dev server  

`npm run prod` - production build

`npm run deploy` - [after doing `firebse init]`]
